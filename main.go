package main

import (
	"encoding/json"
	"errors"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"

	"github.com/alexeyco/simpletable"
	"github.com/c2h5oh/datasize"
	"github.com/dustin/go-humanize"
	"gitlab.com/kot0/tools"
)

var logger = &tools.Logger{
	File:               "",
	ShowTime:           false,
	ShowSourceCodeLine: false,
	ShowSourceCodePath: false,
}

const FIOFileSize = "8G"
const FIORunTime = "25" // 25
const FIORampTime = "5" // 5

const FIOTestFileFolderPath = "tmp_folder"

var FIOTestFilePath = path.Join(FIOTestFileFolderPath, "fio_test_file")

func main() {
	fioVersion, err := checkFIO()
	tools.OnErrorPanic(err)
	logger.Log("Fio version:", fioVersion)

	if !tools.FileExist(FIOTestFileFolderPath) {
		tools.OnErrorPanic(os.MkdirAll(FIOTestFileFolderPath, 0777))
	}

	var fIOConfigs = []FIOConfig{
		// SEQ
		{
			TimeBased: "1",
			ReadWrite: "read",
			Size:      FIOFileSize,
			Bs:        "1M",
			IoDepth:   "1",
			NumJobs:   "1",
			RunTime:   FIORunTime,
			RampTime:  FIORampTime,
		},
		{
			TimeBased: "1",
			ReadWrite: "read",
			Size:      FIOFileSize,
			Bs:        "1M",
			IoDepth:   "1",
			NumJobs:   "8",
			RunTime:   FIORunTime,
			RampTime:  FIORampTime,
		},

		// RND
		{
			TimeBased: "1",
			ReadWrite: "randread",
			Size:      FIOFileSize,
			Bs:        "4k",
			IoDepth:   "32",
			NumJobs:   "16",
			RunTime:   FIORunTime,
			RampTime:  FIORampTime,
		},
		{
			TimeBased: "1",
			ReadWrite: "randread",
			Size:      FIOFileSize,
			Bs:        "4k",
			IoDepth:   "1",
			NumJobs:   "1",
			RunTime:   FIORunTime,
			RampTime:  FIORampTime,
		},
	}

	fIOConfigs = append(fIOConfigs, FIOConfigReverseReadWrite(fIOConfigs)...)

	var fIOBenchmarkResults FIOBenchmarkResults

	for _, fIOConfig := range fIOConfigs {
		logger.Log("Bechmarking config:", tools.ToJson(fIOConfig))

		tools.TryDeleteFile(FIOTestFilePath)
		time.Sleep(10 * time.Second)
		fIOOutput, err := runFIOBenchmark(fIOConfig)
		tools.OnErrorPanic(err)
		tools.TryDeleteFile(FIOTestFilePath)

		results := fIOOutput.getResults()
		fIOBenchmarkResults = append(fIOBenchmarkResults, results)

		logger.Log(fIOBenchmarkResults.generateTable())
	}
}

type FIOConfig struct {
	TimeBased string
	ReadWrite string
	Size      string
	Bs        string
	IoDepth   string
	NumJobs   string
	RunTime   string
	RampTime  string
}

type FIOBenchmarkResult struct {
	ReadWrite string
	Size      string
	Bs        string
	Threads   string
	IoDepth   string

	IOPS           int
	IOSpeedInBytes int
	IOSizeInBytes  int
}

type FIOBenchmarkResults []*FIOBenchmarkResult

func (fIOBenchmarkResults FIOBenchmarkResults) generateTable() string {
	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Bench type"},
			{Align: simpletable.AlignCenter, Text: "File size"},
			{Align: simpletable.AlignCenter, Text: "Block size"},
			{Align: simpletable.AlignCenter, Text: "Threads"},
			{Align: simpletable.AlignCenter, Text: "Io depth"},
			{Align: simpletable.AlignCenter, Text: "IOPS"},
			{Align: simpletable.AlignCenter, Text: "IO speed"},
			{Align: simpletable.AlignCenter, Text: "IO size"},
		},
	}

	for _, fIOBenchmarkResult := range fIOBenchmarkResults {
		var tableCell []*simpletable.Cell

		switch fIOBenchmarkResult.ReadWrite {
		case "randread":
			tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: "Random read"})
		case "randwrite":
			tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: "Random write"})
		case "read":
			tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: "Linear read"})
		case "write":
			tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: "Linear write"})
		default:
			tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: "ERROR!"})
		}

		tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: fIOBenchmarkResult.Size})
		tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: fIOBenchmarkResult.Bs})
		tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: fIOBenchmarkResult.Threads})
		tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: fIOBenchmarkResult.IoDepth})

		tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: humanize.Comma(int64(fIOBenchmarkResult.IOPS))})
		tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: humanize.Comma(int64(int(datasize.ByteSize(fIOBenchmarkResult.IOSpeedInBytes).MBytes()))) + " MB/s"})
		tableCell = append(tableCell, &simpletable.Cell{Align: simpletable.AlignCenter, Text: humanize.Comma(int64(int(datasize.ByteSize(fIOBenchmarkResult.IOSizeInBytes).MBytes()))) + " MB"})

		table.Body.Cells = append(table.Body.Cells, tableCell)
	}

	return table.String()
	// var str []string
	//
	// switch fIOBenchmarkResult.ReadWrite {
	// case "randread":
	// 	str = append(str, "RR")
	// case "randwrite":
	// 	str = append(str, "RW")
	// case "read":
	// 	str = append(str, "R")
	// case "write":
	// 	str = append(str, "W")
	// default:
	// 	str = append(str, "ERROR!")
	// }
	//
	// str = append(str, "SIZE:"+fIOBenchmarkResult.Size)
	// str = append(str, "BS:"+fIOBenchmarkResult.Bs)
	// str = append(str, "Q:"+fIOBenchmarkResult.IoDepth)
	// str = append(str, "T:"+fIOBenchmarkResult.Threads)
	//
	// str = append(str, "IOPS:"+humanize.Comma(int64(fIOBenchmarkResult.IOPS)))
	// str = append(str, "IO size:"+humanize.Bytes(uint64(fIOBenchmarkResult.IOSizeInBytes)))
	// str = append(str, "IO speed:"+humanize.Bytes(uint64(fIOBenchmarkResult.IOSpeedInBytes))+"/s")
	//
	// return strings.Join(str, " ")
}

func (fIOOutput *FIOOutputJsonStruct) getResults() *FIOBenchmarkResult {
	var fIOBenchmarkResult = FIOBenchmarkResult{}

	fIOBenchmarkResult.ReadWrite = fIOOutput.Jobs[0].JobOptions.Rw
	fIOBenchmarkResult.Size = fIOOutput.Jobs[0].JobOptions.Size
	fIOBenchmarkResult.Bs = fIOOutput.Jobs[0].JobOptions.Bs
	fIOBenchmarkResult.Threads = fIOOutput.Jobs[0].JobOptions.Numjobs
	fIOBenchmarkResult.IoDepth = fIOOutput.Jobs[0].JobOptions.Iodepth

	// read
	if fIOOutput.Jobs[0].Read.IoBytes != 0 {
		fIOBenchmarkResult.IOSizeInBytes = fIOOutput.Jobs[0].Read.IoBytes
		fIOBenchmarkResult.IOPS = int(fIOOutput.Jobs[0].Read.Iops)
		fIOBenchmarkResult.IOSpeedInBytes = fIOOutput.Jobs[0].Read.BwBytes
	}

	// write
	if fIOOutput.Jobs[0].Write.IoBytes != 0 {
		fIOBenchmarkResult.IOSizeInBytes = fIOOutput.Jobs[0].Write.IoBytes
		fIOBenchmarkResult.IOPS = int(fIOOutput.Jobs[0].Write.Iops)
		fIOBenchmarkResult.IOSpeedInBytes = fIOOutput.Jobs[0].Write.BwBytes
	}

	return &fIOBenchmarkResult
}

func runFIOBenchmark(fIOConfig FIOConfig) (*FIOOutputJsonStruct, error) {
	output, err := exec.Command("fio",
		"--randrepeat=0",
		"--direct=1",
		"--gtod_reduce=1",
		"--name=T",
		"--time_based="+fIOConfig.TimeBased,
		"--filename="+FIOTestFilePath,
		"--group_reporting=1",
		"--readwrite="+fIOConfig.ReadWrite,
		"--size="+fIOConfig.Size,
		"--bs="+fIOConfig.Bs,
		"--iodepth="+fIOConfig.IoDepth,
		"--numjobs="+fIOConfig.NumJobs,
		"--runtime="+fIOConfig.RunTime,
		"--ramp_time="+fIOConfig.RampTime,
		"--output-format=json",
	).CombinedOutput()
	if tools.CheckError(err) {
		return nil, err
	}

	outputString := string(output)
	outputString = strings.ReplaceAll(outputString, `fio: this platform does not support process shared mutexes, forcing use of threads. Use the 'thread' option to get rid of this warning.`, ``)

	// logger.Log(outputString)

	var fIOOutput FIOOutputJsonStruct
	err = json.Unmarshal([]byte(outputString), &fIOOutput)
	if tools.CheckError(err) {
		return nil, err
	}

	return &fIOOutput, nil
}

type FIOOutputJsonStruct struct {
	FioVersion    string `json:"fio version"`
	Timestamp     int    `json:"timestamp"`
	TimestampMs   int64  `json:"timestamp_ms"`
	Time          string `json:"time"`
	GlobalOptions struct {
		Randrepeat string `json:"randrepeat"`
		Direct     string `json:"direct"`
		GtodReduce string `json:"gtod_reduce"`
	} `json:"global options"`
	Jobs []struct {
		Jobname    string `json:"jobname"`
		Groupid    int    `json:"groupid"`
		Error      int    `json:"error"`
		Eta        int    `json:"eta"`
		Elapsed    int    `json:"elapsed"`
		JobOptions struct {
			Name           string `json:"name"`
			TimeBased      string `json:"time_based"`
			Filename       string `json:"filename"`
			GroupReporting string `json:"group_reporting"`
			Rw             string `json:"rw"`
			Size           string `json:"size"`
			Bs             string `json:"bs"`
			Iodepth        string `json:"iodepth"`
			Numjobs        string `json:"numjobs"`
			Runtime        string `json:"runtime"`
			RampTime       string `json:"ramp_time"`
		} `json:"job options"`
		Read struct {
			IoBytes  int     `json:"io_bytes"`
			IoKbytes int     `json:"io_kbytes"`
			BwBytes  int     `json:"bw_bytes"`
			Bw       int     `json:"bw"`
			Iops     float64 `json:"iops"`
			Runtime  int     `json:"runtime"`
			TotalIos int     `json:"total_ios"`
			ShortIos int     `json:"short_ios"`
			DropIos  int     `json:"drop_ios"`
			SlatNs   struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"slat_ns"`
			ClatNs struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"clat_ns"`
			LatNs struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"lat_ns"`
			BwMin       int     `json:"bw_min"`
			BwMax       int     `json:"bw_max"`
			BwAgg       float64 `json:"bw_agg"`
			BwMean      float64 `json:"bw_mean"`
			BwDev       float64 `json:"bw_dev"`
			BwSamples   int     `json:"bw_samples"`
			IopsMin     int     `json:"iops_min"`
			IopsMax     int     `json:"iops_max"`
			IopsMean    float64 `json:"iops_mean"`
			IopsStddev  float64 `json:"iops_stddev"`
			IopsSamples int     `json:"iops_samples"`
		} `json:"read"`
		Write struct {
			IoBytes  int     `json:"io_bytes"`
			IoKbytes int     `json:"io_kbytes"`
			BwBytes  int     `json:"bw_bytes"`
			Bw       int     `json:"bw"`
			Iops     float64 `json:"iops"`
			Runtime  int     `json:"runtime"`
			TotalIos int     `json:"total_ios"`
			ShortIos int     `json:"short_ios"`
			DropIos  int     `json:"drop_ios"`
			SlatNs   struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"slat_ns"`
			ClatNs struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"clat_ns"`
			LatNs struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"lat_ns"`
			BwMin       int     `json:"bw_min"`
			BwMax       int     `json:"bw_max"`
			BwAgg       float64 `json:"bw_agg"`
			BwMean      float64 `json:"bw_mean"`
			BwDev       float64 `json:"bw_dev"`
			BwSamples   int     `json:"bw_samples"`
			IopsMin     int     `json:"iops_min"`
			IopsMax     int     `json:"iops_max"`
			IopsMean    float64 `json:"iops_mean"`
			IopsStddev  float64 `json:"iops_stddev"`
			IopsSamples int     `json:"iops_samples"`
		} `json:"write"`
		Trim struct {
			IoBytes  int     `json:"io_bytes"`
			IoKbytes int     `json:"io_kbytes"`
			BwBytes  int     `json:"bw_bytes"`
			Bw       int     `json:"bw"`
			Iops     float64 `json:"iops"`
			Runtime  int     `json:"runtime"`
			TotalIos int     `json:"total_ios"`
			ShortIos int     `json:"short_ios"`
			DropIos  int     `json:"drop_ios"`
			SlatNs   struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"slat_ns"`
			ClatNs struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"clat_ns"`
			LatNs struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"lat_ns"`
			BwMin       int     `json:"bw_min"`
			BwMax       int     `json:"bw_max"`
			BwAgg       float64 `json:"bw_agg"`
			BwMean      float64 `json:"bw_mean"`
			BwDev       float64 `json:"bw_dev"`
			BwSamples   int     `json:"bw_samples"`
			IopsMin     int     `json:"iops_min"`
			IopsMax     int     `json:"iops_max"`
			IopsMean    float64 `json:"iops_mean"`
			IopsStddev  float64 `json:"iops_stddev"`
			IopsSamples int     `json:"iops_samples"`
		} `json:"trim"`
		Sync struct {
			LatNs struct {
				Min    int     `json:"min"`
				Max    int     `json:"max"`
				Mean   float64 `json:"mean"`
				Stddev float64 `json:"stddev"`
			} `json:"lat_ns"`
			TotalIos int `json:"total_ios"`
		} `json:"sync"`
		JobRuntime   int     `json:"job_runtime"`
		UsrCpu       float64 `json:"usr_cpu"`
		SysCpu       float64 `json:"sys_cpu"`
		Ctx          int     `json:"ctx"`
		Majf         int     `json:"majf"`
		Minf         int     `json:"minf"`
		IodepthLevel struct {
			Field1 float64 `json:"1"`
			Field2 float64 `json:"2"`
			Field3 float64 `json:"4"`
			Field4 float64 `json:"8"`
			Field5 float64 `json:"16"`
			Field6 float64 `json:"32"`
			Field7 float64 `json:">=64"`
		} `json:"iodepth_level"`
		IodepthSubmit struct {
			Field1 float64 `json:"0"`
			Field2 float64 `json:"4"`
			Field3 float64 `json:"8"`
			Field4 float64 `json:"16"`
			Field5 float64 `json:"32"`
			Field6 float64 `json:"64"`
			Field7 float64 `json:">=64"`
		} `json:"iodepth_submit"`
		IodepthComplete struct {
			Field1 float64 `json:"0"`
			Field2 float64 `json:"4"`
			Field3 float64 `json:"8"`
			Field4 float64 `json:"16"`
			Field5 float64 `json:"32"`
			Field6 float64 `json:"64"`
			Field7 float64 `json:">=64"`
		} `json:"iodepth_complete"`
		LatencyNs struct {
			Field1  float64 `json:"2"`
			Field2  float64 `json:"4"`
			Field3  float64 `json:"10"`
			Field4  float64 `json:"20"`
			Field5  float64 `json:"50"`
			Field6  float64 `json:"100"`
			Field7  float64 `json:"250"`
			Field8  float64 `json:"500"`
			Field9  float64 `json:"750"`
			Field10 float64 `json:"1000"`
		} `json:"latency_ns"`
		LatencyUs struct {
			Field1  float64 `json:"2"`
			Field2  float64 `json:"4"`
			Field3  float64 `json:"10"`
			Field4  float64 `json:"20"`
			Field5  float64 `json:"50"`
			Field6  float64 `json:"100"`
			Field7  float64 `json:"250"`
			Field8  float64 `json:"500"`
			Field9  float64 `json:"750"`
			Field10 float64 `json:"1000"`
		} `json:"latency_us"`
		LatencyMs struct {
			Field1  float64 `json:"2"`
			Field2  float64 `json:"4"`
			Field3  float64 `json:"10"`
			Field4  float64 `json:"20"`
			Field5  float64 `json:"50"`
			Field6  float64 `json:"100"`
			Field7  float64 `json:"250"`
			Field8  float64 `json:"500"`
			Field9  float64 `json:"750"`
			Field10 float64 `json:"1000"`
			Field11 float64 `json:"2000"`
			Field12 float64 `json:">=2000"`
		} `json:"latency_ms"`
		LatencyDepth      int     `json:"latency_depth"`
		LatencyTarget     int     `json:"latency_target"`
		LatencyPercentile float64 `json:"latency_percentile"`
		LatencyWindow     int     `json:"latency_window"`
	} `json:"jobs"`
}

func checkFIO() (string, error) {
	output, err := exec.Command("fio", "-version").CombinedOutput()
	if tools.CheckError(err) {
		return "", err
	}

	outputString := string(output)
	outputString = tools.RemoveNewlineCharsFromString(outputString)

	if !strings.Contains(outputString, "fio-") {
		return "", errors.New("fio not installed")
	}

	return outputString, nil
}

func FIOConfigReverseReadWrite(fIOConfigs []FIOConfig) []FIOConfig {
	var fIOConfigsReturn []FIOConfig

	for _, fIOConfig := range fIOConfigs {
		if fIOConfig.ReadWrite == "read" {
			fIOConfig.ReadWrite = "write"
		} else {
			if fIOConfig.ReadWrite == "write" {
				fIOConfig.ReadWrite = "read"
			}
		}

		if fIOConfig.ReadWrite == "randread" {
			fIOConfig.ReadWrite = "randwrite"
		} else {
			if fIOConfig.ReadWrite == "randwrite" {
				fIOConfig.ReadWrite = "randread"
			}
		}

		fIOConfigsReturn = append(fIOConfigsReturn, fIOConfig)
	}

	return fIOConfigsReturn
}
