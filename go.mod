module ioBenchmark

go 1.18

require (
	github.com/alexeyco/simpletable v1.0.0
	github.com/c2h5oh/datasize v0.0.0-20220606134207-859f65c6625b
	github.com/dustin/go-humanize v1.0.0
	github.com/shirou/gopsutil v3.21.11+incompatible
	github.com/spf13/cast v1.5.0
	gitlab.com/kot0/tools v1.2.1
)

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/m1ome/randstr v0.0.0-20170328115817-50e7f2dc0288 // indirect
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/rivo/uniseg v0.1.0 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/tklauser/numcpus v0.4.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
)
