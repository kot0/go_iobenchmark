# Build
FROM golang:bullseye AS build

WORKDIR /build

COPY . .

#GOAMD64=v1 (default): The baseline. Exclusively generates instructions that all 64-bit x86 processors can execute.
#GOAMD64=v2: all v1 instructions, plus CMPXCHG16B, LAHF, SAHF, POPCNT, SSE3, SSE4.1, SSE4.2, SSSE3.
#GOAMD64=v3: all v2 instructions, plus AVX, AVX2, BMI1, BMI2, F16C, FMA, LZCNT, MOVBE, OSXSAVE.
#GOAMD64=v4: all v3 instructions, plus AVX512F, AVX512BW, AVX512CD, AVX512DQ, AVX512VL.
ENV GOAMD64=v1
RUN go build -o app .

# Deploy
FROM alpine

RUN apk update && apk add fio

WORKDIR /app

COPY --from=build /build/app /app/app

ENTRYPOINT ["./app"]


